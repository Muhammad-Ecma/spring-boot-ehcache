package ai.ecma.springcaching.config;

import ai.ecma.springcaching.model.ApiResponse;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import java.time.Duration;

@Configuration
@EnableCaching
public class AppConfig {


    @Bean
    public CacheManager ehCacheManager() {
        CachingProvider provider = Caching.getCachingProvider();
        CacheManager cacheManager = provider.getCacheManager();

        CacheConfiguration<?, ?> configuration =
                CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class, resourcePoolsBuilder())
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofDays(5))) //Cache timeout
                        .withSizeOfMaxObjectGraph(100)  //Traversal depth of object graph when object size is counted
                        .withSizeOfMaxObjectSize(100, MemoryUnit.MB)     //Maximum object size that can be cached
//                        .add(CacheEventListenerConfigurationBuilder.newEventListenerConfiguration(    //Add listener
//                                new EhCacheEventListener(), EventType.EXPIRED).unordered().asynchronous())
                        .build();

        javax.cache.configuration.Configuration<?, ?> stringDoubleConfiguration =
                Eh107Configuration.fromEhcacheCacheConfiguration(configuration);

        cacheManager.createCache("students", stringDoubleConfiguration);
        return cacheManager;
    }

    private ResourcePoolsBuilder resourcePoolsBuilder() {
        return ResourcePoolsBuilder.newResourcePoolsBuilder()
                .heap(10, MemoryUnit.KB)        //Heap cache size
                .offheap(10, MemoryUnit.MB);    //Out of heap cache size
//                .disk(1, MemoryUnit.MB);        //File cache size
    }
}