package ai.ecma.springcaching.service;

import ai.ecma.springcaching.entity.Country;
import ai.ecma.springcaching.model.ApiResponse;
import ai.ecma.springcaching.repository.CountryRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import javax.cache.annotation.CacheResult;

/**
 * Author: Muhammad Mo'minov
 * 08.06.2021
 */
@Service
@AllArgsConstructor
public class CountryService {
    final CountryRepository countryRepository;

    @CacheResult(cacheName = "country" /*, key = "#id" , sync = true, condition = "#id <= 100", unless = "#result.id != 1"*/)
    public ApiResponse getOne(int id) {
        sleep();
        return ApiResponse.success(countryRepository.findById(id).orElseGet(Country::new));
    }

    @CachePut(value = "country", key = "#id")
    public ApiResponse update(int id, Country country) {
        Country one = countryRepository.getOne(id);
        one.setName(country.getName());
        one.setNiceName(country.getNiceName());
        one.setPhoneCode(country.getPhoneCode());
        return ApiResponse.success(countryRepository.save(one));
    }

    @CachePut(value = "country", key = "#country.id")
    public ApiResponse save(Country country) {
        return ApiResponse.success(countryRepository.save(country));
    }

    @CacheEvict(value = "country", key = "#id")
    public ApiResponse deleteOne(int id) {
        countryRepository.deleteById(id);
        return ApiResponse.success("Successfully deleted!");
    }

    @CacheEvict(value = "country", allEntries = true)
    public ApiResponse deleteAll() {
        countryRepository.deleteAll();
        return ApiResponse.success("Successfully deleted!");
    }

    public void sleep() {
        try {
            System.err.println("I am sleeping..........");
            Thread.sleep(4000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}